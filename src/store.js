import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

let { names } = require('./names')

export default new Vuex.Store({
  state: {
    skills: []
  },
  mutations: {
    [names.SET_SKILLS] (state, items) {
      state.skills = items
    }
  },
  actions: {
    async [names.SET_SKILLS] ({ commit }) {
      commit(names.SET_SKILLS, await fetch('/skills.json').then(r => r.json()))
    }
  }
})
